# Epidemiologiska data för Standardiserade vårdförlopp (SVF)

## Förberedelser

Börja med att installera ett par RCC-specifika paket:

```
devtools::install_bitbucket("cancercentrum/incadata")
devtools::install_bitbucket("cancercentrum/incasurv")
```

Därutöver krävs cancerdata som underlag.
För tidigare versioner användes RTR-data, varför skripten anpassats för detta.
Då denna data numera kan hämtas från INCA, 
kan viss anpassning av variabelnamn etcetera krävas.


## Struktur

Projektets struktur bygger på [ProjectTemplate](http://projecttemplate.net/).
Dt rekommenderas att först ta del av grunderna för hur ett sådant projekt är uppbyggt!

## Uppdatera med fler diagnoser
I stora drag sker detta i följande steg:

* Spara ner ett nytt datauttag i data-mappen.
* Modifiera diagnosvalen via `munge/02-diagnoser.R`. 
* Kör sedan skripten från `src`-mappen för att framställa figurer 
(framförallt `master.R` som genererar grafer för de flesta diagnoser.
* Figurerna skapas med hjälp av funktioner från `lib`-mappen och 
sparas i `graphs`-mappen.

För att köra hela projektet, använd funktionen `ProjectTemplate::run.project`.
