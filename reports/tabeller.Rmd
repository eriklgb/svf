---
title: "Antalstabeller för SVF"
author: "Erik Bülow"
date: "4 november 2016"
output: word_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r, results='asis', echo=FALSE, message=FALSE, warning=FALSE, error=FALSE}
load("data/rtr_diagnoser.rda")
source("funktioner.R")


diag_table <- function(x) {
  
  Incidens <- 
    x %>% 
    filter(
      diagnosår %in% 2011:2015,
      !is.na(område)) %>%
    group_by(diagnos, diagnosår) %>% 
    summarise(antal = n()) %>% {
      a <- .$antal
      names(a) <- .$diagnosår
      a
    }
  
  
  Prevalens <- 
    prevalens_tabell(x) %>% 
    group_by(år) %>% 
    summarise(antal = sum(prevalens)) %>% {
      a <- .$antal
      names(a) <- .$år
      a
    }
  
  t <- as.data.frame(rbind(Incidens, Prevalens))
  t$Median <- apply(t, 1, median)
  
  print(knitr::kable(t, caption = unique(x$diagnos)))
  
  
}

plyr::d_ply(rtr_diagnoser, "diagnos", diag_table)

```


<!-- # Extrauppgifter för lymfom och KLL -->

<!-- ## Incidens per åldersgrupp och kön -->

<!-- > Herman ville även ha en tabell med ålders och könsuppdelning enligt nedan för 2014, går det att lösa? -->

<!-- Jag antar att det är incidens som avses, dvs antal diagnostiserade fall under 2014. -->

<!-- ```{r, echo=FALSE, message=FALSE, eval = FALSE} -->

<!-- rtr_herman <-  filter(rtr_diagnoser, diagnos == "Lymfom och KLL") -->

<!-- rtr_herman %>%  -->
<!--   filter( -->
<!--     diagnosår == 2014, -->
<!--     ålder >= 16 -->
<!--   ) %>%  -->
<!--   transmute( -->
<!--     åldersgrupp =  -->
<!--       rccmisc:::cut.integer(ålder, c(16, 31, 41, 51, 61, 71, 81, 91, 200) - 1), -->
<!--     kön_beskrivning -->
<!--   ) %>%  -->
<!--   group_by(åldersgrupp, kön_beskrivning) %>%  -->
<!--   summarise(antal = n()) %>%  -->
<!--   tidyr::spread(kön_beskrivning, antal) %>%  -->
<!--   knitr::kable(caption = "Antal diagnoser 2014 per kön och åldersgrupp") -->
<!-- ``` -->




<!-- ## Antal anmälda per sjukhus -->

<!-- För kännedom efter önskemål från Herman har jag tagit fram en tabell med antalet fall per anmälande sjukhus. -->
<!-- Observera att den uppgift vi egentligen har i registret är sjukhuskoden "anlas". -->
<!-- Denna har jag sedan "översatt" till klartext med en översättningsnyckel som inte nödvändigtvis är helt up-to-date. -->


<!-- ```{r, echo=FALSE, message=FALSE} -->
<!-- rtr_herman %>%  -->
<!--   group_by(anlas, anlas_beskrivning) %>%  -->
<!--   summarise(Antal = n()) %>%  -->
<!--   ungroup() %>%  -->
<!--   arrange(desc(Antal)) %>%  -->
<!--   knitr::kable(caption = "Antal anmälda per sjukhus") -->
<!-- ``` -->

<!--
## Gruppering av botbara/kroniska

OBS! Listan vi fick tycks använda SNOMED-koder. Baserar vi urvalet på denna kod så får vi följande problem (men se även nedan ang Snomed3):

Listan vi fått täcker inte alla fall som finns i registret. 
Följande tabell visar i första kolumnen gruppen. Sista delen av tabellen har inga värden angivna i den kolumnen (NA = Non Available). Hur ska dessa fall hanteras? Av sista kolumnen (antal fall) ser vi att detta omfattar ett betydande antal fall.

Dessutom anges koderna `959135` respektive `959136`på flera ställen i det ursprungliga Word-dokumentet såsom tillhörandes båda grupperna. Dock saknas observerade fall av dessa Snomed-koder så detta är egentligen inget problem (men se nedan ang Snomed3).


```{r, echo=FALSE, message=FALSE, eval=FALSE}
rtr_herman %>% 
group_by(strata, icdo, snomed, snomed_beskrivning) %>% 
summarise(antal = n()) %>% 
ungroup() %>% 
arrange(strata, desc(antal)) %>% 
knitr::kable(caption = "Antal per grupp och snomed-kod")

```


### Med Snomed3

Om vi istället antar att selektionen avser Snomed3 (vilket inte framgår av Hermans dokument) blir uppdelningen betydligt mer heltäckande (dock fortfarande några koder som inte omfattas).
Dock får vi här problem med koderna `959135` respektive `959136`. (Hur dessa fall grupperas nedan är inte korrekt men inkluderas på detta felaktiga sätt bara för att indikera att problemet inte är negligerbart då det rör sig om relativt många fall).


```{r, echo=FALSE, message=FALSE, eval=FALSE}
rtr_herman %>% 
group_by(strata_snomed3, snomed3, snomed3_beskrivning) %>% 
summarise(antal = n()) %>% 
ungroup() %>% 
arrange(strata_snomed3, desc(antal)) %>% 
knitr::kable(caption = "Antal per grupp och snomed3-kod")

```
-->
