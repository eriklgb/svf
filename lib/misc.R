# Colors to use in the graph
colors_hc <- function() {
  c("#ffb117", "#434348", "#90ed7d","#AAEEEE", "#f15c80",
    "#e4d354", "#2b908f", "#f45b5b", "#7798BF", "#8085e9")
}

# Convert factor or character to numeric
as_numeric <- function(x) as.numeric(as.character(x))

# Basic theme to use for RCC graphs
theme_rcc <- function(){
  ggplot2::theme_bw() +
  theme(
    legend.position = c(1, 0),
    legend.justification = c(1, 0),
    legend.background = element_rect(colour = NA, fill = NA),
    legend.key = element_blank(),
    legend.box.just = "left"
  )
}
