
overlevnad_tabell <- function(x) {
  x %>%
    filter(diagnosår %in% 2011:2015) %>%
    as.rtrsurvdata(censoring_date = "2016-11-04") %>%
    incasurv()
}

overlevnad <- function(x) {
  lp <- if (is.null(x$strata)) "none" else c(0.5, 0.15)
  cols <- c("#00b3f6", "#603311")
  overlevnad_tabell(x) %>%
    plot_km(ci = TRUE) +
    ggtitle("Relativ överlevnad (med 95 % konfidensintervall)") +
    theme(
      legend.position      = lp,
      legend.title         = element_blank(),
      legend.justification = "center",
      legend.text          = element_text(size = 10),
      legend.key.size      = unit(.5, "cm"),
      legend.direction     = "horizontal"
    ) +
    scale_colour_manual(values = cols) +
    scale_fill_manual(values   = cols)
}
